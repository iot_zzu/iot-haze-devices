/*

	2017-12-18 NOW
	BY 楚博豪 王法权
	IN 郑州大学
	雾霾盒子 CONFIG_H文件
	Email：1040459612@qq.com
	
*/



#ifndef __CONFIG_H
#define	__CONFIG_H
#include "stm32f10x.h"
#include <stdio.h>


// 系统时钟配置
void RCC_Configuration(void);

// 硬件延时函数
void delay_ms(u32 ms);

// 硬件延时函数
void delay_us(u32 us);

// 发送一个数组函数
void Usart_SendArray( USART_TypeDef * pUSARTx, uint8_t *array, uint16_t num);
#endif
