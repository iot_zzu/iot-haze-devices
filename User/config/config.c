/*
	2018-05-02 NOW
	BY 楚博豪
	IN 郑州大学物联网实验硬件组
	雾霾盒子 CONFIG_C文件
	Email：1040459612@qq.com
*/
#include "config.h"
#include "stm32f10x_flash.h"

/*******************************************************************************
* 函数名称  :  RCC_Configuration
* 函数描述  :  设置系统各部分时钟
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void RCC_Configuration(void)
{
	  /* 定义枚举类型变量 HSEStartUpStatus */
	  ErrorStatus HSEStartUpStatus;
  	RCC_DeInit();
  	RCC_HSEConfig(RCC_HSE_ON);
  	HSEStartUpStatus = RCC_WaitForHSEStartUp();
  	if(HSEStartUpStatus == SUCCESS)
  	{
    	RCC_HCLKConfig(RCC_SYSCLK_Div1); 
    	RCC_PCLK2Config(RCC_HCLK_Div1); 
    	RCC_PCLK1Config(RCC_HCLK_Div2);
    	FLASH_SetLatency(FLASH_Latency_2);
    	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
    	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
    	RCC_PLLCmd(ENABLE);
    	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
    	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    	while(RCC_GetSYSCLKSource() != 0x08);
  	}	
}

/*******************************************************************************
* 函数名称  :  delay_us()
* 函数描述  :  计时器延时函数
* 输入参数  :  所要延时的时间（ns）
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void delay_us(u32 nus)
{		
		uint32_t i;
		SysTick_Config(72);
		for(i=0; i<nus; i++)
		{
			while( !((SysTick->CTRL) & (1<<16)) );
		}
		SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk; 
}

/*******************************************************************************
* 函数名称  :  SysTick_Delay_ms
* 函数描述  :  计时器延时函数
* 输入参数  :  所要延时的时间（ms）
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void delay_ms(u32 ms)
{
	uint32_t i;
	SysTick_Config(72000);
	for(i=0; i<ms; i++)
  {
		while( !((SysTick->CTRL) & (1<<16)) );
	}
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}

/*******************************************************************************
* 函数名称  :  Usart_SendByte
* 函数描述  :  发送字节函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
static void Usart_SendByte( USART_TypeDef * pUSARTx, uint16_t ch)
{
	USART_SendData(pUSARTx,ch);
	while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);	
}

/*******************************************************************************
* 函数名称  :  Usart_SendArray 11
* 函数描述  :  发送8位数组函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void Usart_SendArray( USART_TypeDef * pUSARTx, uint8_t *array, uint16_t num)
{
  uint8_t i;
	for(i=0; i<num; i++)
  {
	    Usart_SendByte(pUSARTx,array[i]);	 
  }
	while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TC)==RESET);
}


