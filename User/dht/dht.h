/*

	2017-12-18  NOW
	BY 楚博豪 王法权
	IN 郑州大学
	雾霾盒子 TEMPER_H文件
	Email：1040459612@qq.com
	
*/


#ifndef __DHT11_H
#define	__DHT11_H


#include "stm32f10x.h"
#include "config.h"
#include "sys.h"
#include "oled.h"

void DHT11_Read_Data(void);

#endif






