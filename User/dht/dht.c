/*

	2017-12-18  NOW
	BY 楚博豪 王法权
	IN 郑州大学
	雾霾盒子 TEMPER_H文件
	Email：1040459612@qq.com
	
*/
#include "dht.h"
// 设置PA1端口为输入模式
static void DHT11_IN_Init(void) 
{  
	GPIO_InitTypeDef GPIO_InitStructure;   
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	/* 温湿度传感器控制线浮空输入 */  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  
	GPIO_Init(GPIOA, &GPIO_InitStructure);  
	GPIO_SetBits(GPIOA,GPIO_Pin_1);  
}


// 设置PA1端口为输出模式
static void DHT11_OUT_Init(void) 
{  
	GPIO_InitTypeDef GPIO_InitStructure;  
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);  
	/* 温湿度传感器控制线模拟输入 */  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_Init(GPIOA, &GPIO_InitStructure); 
	GPIO_SetBits(GPIOA,GPIO_Pin_1); 
}


// 初始化DHT11
static void DHT11_Rst(void) 
{    
	DHT11_OUT_Init();    //设置引脚为输出模式    
	PAout(1) = 0;   
	delay_ms(19);        //拉低至少18ms    
	PAout(1) = 1;          //总线拉高20~40us,DHT11会返回一个低电平
	delay_us(30);    
	DHT11_IN_Init();      //设置管脚为输入模式，用于读取DHT11的值,此时PAin(1)为高电平  
}


// 检测DHT11,检测成功返回0，否则返回1
static uint8_t DHT11_Check(void) 
{    
	return PAin(1);        //检测输入电平高低 
} 


// 读PA1端口电平
static uint8_t DHT11_Read_Bit(void) 
{    
	while(!PAin(1));   //等待变高电平,每次发送数据前都会有一个50us的电 平信号        
	delay_us(40);    //等待40us   
	if(PAin(1) == 1)   
	{  
		while(PAin(1));  //当数据为1时，还得等到1信号结束，这里有不少人会出错   
		return 1;    
	}    
	else
	{		
		return 0;  
	}		
}


// 从DHT11读取一个字节
static uint8_t DHT11_Read_Byte(void)     
{           
	uint8_t i;    
	uint8_t Data = 0;   
	for(i=0;i<8;i++)       
	{           
		Data <<= 1;      
		Data |= DHT11_Read_Bit();        
	}      
	return Data; 
}


// 从DHT11读取一个8位数据包
void DHT11_Read_Data()
{
	uint8_t i = 0;
	uint8_t Data_Buff[5];
	
	DHT11_Rst();
	if (DHT11_Check() == 0)     //当检测到DHT11给STM32主控芯片的输入 电压为低电压时，说明DHT11开始响应    
	{      
		while(!PAin(1));       //等待80us的低电平响应信号结束  
		while(PAin(1));         //等待80us的高电平结束，开始接收数据        
		for (i = 0; i < 5; i++)      
		{        
			Data_Buff[i] = DHT11_Read_Byte();  
		} 
		
		while(!PAin(1)); 
		
		DHT11_OUT_Init();
		PAout(1) = 1;
		
		if ((Data_Buff[0]+Data_Buff[1]+Data_Buff[2]+Data_Buff[3]) == Data_Buff[4])
		{
			
			OLED_ShowString(0,3,"TE: ");
			OLED_ShowNum(25,3, Data_Buff[2]/10,1,1);
			OLED_ShowNum(35,3, Data_Buff[2]%10,1,1);
			OLED_ShowString(45,3,"^C");
			
			OLED_ShowString(70,3,"HU: ");
			OLED_ShowNum(95,3, Data_Buff[0]/10,1,1);
			OLED_ShowNum(105,3, Data_Buff[0]%10,1,1);
			OLED_ShowString(115,3,"%");	
		}
		else
		{
			OLED_ShowString(0,0,"Error Data");
		}
	}
	else
	{
		OLED_ShowString(0,0,"Device Not Available");
	}
}
