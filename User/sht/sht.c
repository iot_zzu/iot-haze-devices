/*
	2018-05-02 NOW
	BY 楚博豪
	IN 郑州大学物联网实验硬件组
	雾霾盒子 SHT_C文件
	Email：1040459612@qq.com
*/

#include "sht.h"
#include<stdio.h>
SHT2x_data SHT20;

/*******************************************************************************
* 函数名称  :  SHT2x_Init
* 函数描述  :  初始化SHT20
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
uint8_t SHT2x_Init(void)
{
	u8 err;
	IIC_Init();
	err = SHT2x_SoftReset();
	return err;
}

/*******************************************************************************
* 函数名称  :  SHT2x_SoftReset
* 函数描述  :  SHT20软件复位
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
uint8_t SHT2x_SoftReset(void) //SHT20软件复位
{
	u8 err=0;
	IIC_Start();
  IIC_Send_Byte(0x80);
	err = IIC_Wait_Ack();
	IIC_Send_Byte(0xFE);
	err = IIC_Wait_Ack();
	IIC_Stop();
	return err;
}

/*******************************************************************************
* 函数名称  :  SHT2x_GetTempPoll
* 函数描述  :  获取温度值
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
float SHT2x_GetTempPoll(void)
{
    float TEMP;
    u8 ack, tmp1, tmp2;
    u16 ST;
    u16 i=0;
    IIC_Start();									//发送IIC开始信号
    IIC_Send_Byte(I2C_ADR_W);			//IIC发送一个字节 
		ack = IIC_Wait_Ack();	
		IIC_Send_Byte(TRIG_TEMP_MEASUREMENT_POLL);
		ack = IIC_Wait_Ack();
    do 
		{
			delay_ms(100);               
			IIC_Start();								//发送IIC开始信号
			IIC_Send_Byte(I2C_ADR_R);	
			i++;
			ack = IIC_Wait_Ack();
			if(i==1000)break;
    } while(ack!=0);
    tmp1 = IIC_Read_Byte(1);
    tmp2 = IIC_Read_Byte(1);
		IIC_Read_Byte(0);
    IIC_Stop();

    ST = (tmp1 << 8) | (tmp2 << 0);
    ST &= ~0x0003;
    TEMP = ((float)ST * 0.00268127) - 46.85;
    return (TEMP);	  
}

/*******************************************************************************
* 函数名称  :  SHT2x_GetHumiPoll
* 函数描述  :  获取湿度
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
float SHT2x_GetHumiPoll(void)
{
    float HUMI;
    u8 ack, tmp1, tmp2;
    u16 SRH;
		u16 i=0;
    
    IIC_Start();									//发送IIC开始信号
    IIC_Send_Byte(I2C_ADR_W);			//IIC发送一个字节 
		ack = IIC_Wait_Ack();	
		IIC_Send_Byte(TRIG_HUMI_MEASUREMENT_POLL);
		ack = IIC_Wait_Ack();    
    do 
		{
			delay_ms(100);               
			IIC_Start();				//发送IIC开始信号
			IIC_Send_Byte(I2C_ADR_R);	
			i++;
			ack = IIC_Wait_Ack();
			if(i==100)
				break;
    } while(ack!=0);
    
    tmp1 = IIC_Read_Byte(1);
   
    tmp2 = IIC_Read_Byte(1);
    IIC_Read_Byte(0);
    IIC_Stop();
    
    SRH = (tmp1 << 8) | (tmp2 << 0);
    SRH &= ~0x0003;
    HUMI = ((float)SRH * 0.00190735) - 6;
    return (HUMI);
}
