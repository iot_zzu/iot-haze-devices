/*
	2018-05-02 NOW
	BY 楚博豪
	IN 郑州大学物联网实验硬件组
	雾霾盒子 MAIN_C文件
	Email：1040459612@qq.com
*/
#include "stm32f10x.h"
#include "config.h"
#include "sys.h"
#include "dht.h"
#include "sht.h"
#include "pm.h"
#include "wifi.h"
#include "bmp.h"

extern uint8_t USART2_RX_FLAG;
/*******************************************************************************
* 主函数		:	主函数
*******************************************************************************/
int main(void)
{	
	USART2_Config();
	USART3_Config();
	OLED_Init();
	SHT2x_Init();

	USART_GetFlagStatus(USART2,USART_FLAG_TC);
	USART_GetFlagStatus(USART3,USART_FLAG_TC);

	OLED_Clear();
	OLED_DrawBMP(0,0,88,7,BMP1); 
	OLED_DrawBMP(0,7,128,8,BMP2);
	delay_ms(4000);
	jg_open();
	delay_ms(100);
	OLED_Clear();
	
	while(1)
	{
		navigation();
		jg_readdata();
		delay_ms(500);
		PmCheck();
	}
	
}
