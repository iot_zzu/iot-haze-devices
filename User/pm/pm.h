/*

	2017-12-18 NOW
	BY 楚博豪 王法权
	IN 郑州大学
	雾霾盒子 CONFIG_H文件
	Email：1040459612@qq.com
	
*/

#ifndef __PM_H
#define	__PM_H
#include "stm32f10x_flash.h"
#include "config.h"
#include "oled.h"
#include "sht.h"


void USART3_Config(void);
void PmCheck(void);
void jg_open(void);
void jg_readdata(void);
void jg_off(void);

#endif
