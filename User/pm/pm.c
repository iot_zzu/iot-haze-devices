/*
	2018-05-02 NOW
	BY 楚博豪
	IN 郑州大学物联网实验硬件组
	雾霾盒子 PM_C文件
	Email：1040459612@qq.com
*/
#include "pm.h"
char SHT_Buf[32];
char PM_Buf[32];
uint16_t hor=0;
uint8_t PM25_data[2] = {0};
uint8_t PM10_data[2] = {0};
uint16_t HR_crc = 0;
uint16_t check = 0;
uint8_t SHOW[15] = {0};
uint16_t USART_RX_STB=0;      
uint16_t complete_flag=1;
uint8_t USART_RX_BUF[64];
uint16_t icount;

/*******************************************************************************
* 函数名称  :  NVIC_Configuration
* 函数描述  :  串口1中断配置函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
static void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* 函数名称  :  USART3_Config
* 函数描述  :  串口3配置函数 雾霾检测仪
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void USART3_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	NVIC_Configuration();
	
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure);
	
	NVIC_Configuration();
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);	
	USART_Cmd(USART3, ENABLE);
}

/*******************************************************************************
* 函数名称  :  jg_open
* 函数描述  :  雾霾盒子开机指令发送函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void jg_open(void)
{
  uint8_t jg_openstr[9];
	jg_openstr[0]=0xAA;
	jg_openstr[1]=0x01;
	jg_openstr[2]=0x00;
	jg_openstr[3]=0x00;
	jg_openstr[4]=0x00;
	jg_openstr[5]=0x00;
	jg_openstr[8]=0xBB;
	hor=0;
	hor=jg_openstr[0]+jg_openstr[1]+jg_openstr[2]+jg_openstr[3]+jg_openstr[4]+jg_openstr[5]+jg_openstr[8];
	jg_openstr[6]=hor/256;
	jg_openstr[7]=hor%256;
		
	Usart_SendArray(USART3,jg_openstr,9);
} 

/*******************************************************************************
* 函数名称  :  jg_readdata
* 函数描述  :  雾霾盒子读数据指令发送函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void jg_readdata(void)
{
  uint8_t jg_readdatastr[9];
	jg_readdatastr[0]=0xAA;
	jg_readdatastr[1]=0x02;
	jg_readdatastr[2]=0x00;
	jg_readdatastr[3]=0x00;
	jg_readdatastr[4]=0x00;
	jg_readdatastr[5]=0x00;
	jg_readdatastr[8]=0xBB;
	hor=0;
	hor=jg_readdatastr[0]+jg_readdatastr[1]+jg_readdatastr[2]+jg_readdatastr[3]+jg_readdatastr[4]+jg_readdatastr[5]+jg_readdatastr[8];
	jg_readdatastr[6]=hor/256;
	jg_readdatastr[7]=hor%256;
	
	Usart_SendArray(USART3,jg_readdatastr,9);
}



/*******************************************************************************
* 函数名称  :  jg_off
* 函数描述  :  雾霾盒子关机指令函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void jg_off(void)
{
  uint8_t jg_offstr[9];
	jg_offstr[0]=0xAA;
	jg_offstr[1]=0x03;
	jg_offstr[2]=0x00;
	jg_offstr[3]=0x00;
	jg_offstr[4]=0x00;
	jg_offstr[5]=0x00;
	jg_offstr[8]=0xBB;
	hor=0;
	hor=jg_offstr[0]+jg_offstr[1]+jg_offstr[2]+jg_offstr[3]+jg_offstr[4]+jg_offstr[5]+jg_offstr[8];
	jg_offstr[6]=hor/256;
	jg_offstr[7]=hor%256;
	
	Usart_SendArray(USART3,jg_offstr,9);
}



/*******************************************************************************
* 函数名称  :  show_picture
* 函数描述  :  雾霾盒子数据解析函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
static void show_picture(void)
{
	uint16_t PM25,PM10;
	//uint16_t sss;


	PM25 = ((u16)PM25_data[0]<<8)+(u16)PM25_data[1];
	PM10 = ((u16)PM10_data[0]<<8)+(u16)PM10_data[1];

	if(PM25>999)
	{
		PM25=999;
	}
	if(PM10>1500)
	{
		PM10=1500;
	}
	
	//	PM25显示
	SHOW[2] = PM25;
	//	PM10显示
	SHOW[3] = PM10;
}



/*******************************************************************************
* 函数名称  :  FucCheckSum
* 函数描述  :  数据校验函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
static uint16_t FucCheckSum(u8 dd[])
{
	uint8_t j;
	uint16_t tempq=0;
	for(j=0;j<=5;j++)
	{
		tempq+=dd[j];
	}
	tempq=tempq+dd[8];
	return(tempq);
}


/*******************************************************************************
* 函数名称  :  PmCheck
* 函数描述  :  PM25/PM10/TEMPERATURE/HUMIDITY显示
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void PmCheck(void)
{
		float temp,humi;
		OLED_ShowString(0,2,"PM25:");
		OLED_ShowString(0,6,"PM10:");
		if(complete_flag==1)
		{
				USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
				HR_crc = FucCheckSum(USART_RX_BUF);//校验
				check = ((u16)USART_RX_BUF[6]<<8)+USART_RX_BUF[7];
			
				if((HR_crc==check)) //判断接收的数据是否正确
				{
						if(USART_RX_BUF[1]==0x02)
						{
								PM10_data[0] = USART_RX_BUF[2];
								PM10_data[1] = USART_RX_BUF[3];
								PM25_data[0] = USART_RX_BUF[4];
								PM25_data[1] = USART_RX_BUF[5];
								show_picture();
								sprintf(PM_Buf,"%d",SHOW[2]);
								OLED_ShowString(45,2,(u8 *)PM_Buf);

								sprintf(PM_Buf,"%d",SHOW[3]);
								OLED_ShowString(45,6,(u8 *)PM_Buf);

						}
				}
				else
				{
					OLED_ShowString(60,2,"Err Data!");
				}
				USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
				complete_flag=0;		
		}
		else
		{
			OLED_ShowString(60,2,"No");
		}
		temp=SHT2x_GetTempPoll();//获取SHT20 温度
		humi=SHT2x_GetHumiPoll();//获取SHT20 湿度
		
		// get temperature
		SHOW[0]=(int)temp;
		// get humidness
		SHOW[1]=(int)humi;
		
		sprintf(SHT_Buf,"%d^C",SHOW[0]);
		OLED_ShowString(85,2,(u8 *)SHT_Buf);
		sprintf(SHT_Buf,"%d^C",SHOW[1]);
		OLED_ShowString(85,6,(u8 *)SHT_Buf);
		
		
		
		Usart_SendArray(USART2,SHOW,15);
}

/*******************************************************************************
* 函数名称  :  USART3_IRQHandler
* 函数描述  :  串口3中断处理函数
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void USART3_IRQHandler(void)
{
		uint8_t res;	
		if(USART_GetITStatus(USART3,USART_IT_RXNE)!=RESET)
		{
				res=USART_ReceiveData(USART3);
				USART_RX_BUF[USART_RX_STB]=res;		

				USART_RX_STB++;
			  if(USART_RX_STB==9)
				{
						complete_flag=1;	
						USART_RX_STB=0;
				}					
		} 	
}
