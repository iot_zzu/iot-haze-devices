/*

	2017-12-18  NOW
	BY 楚博豪 王法权
	IN 郑州大学
	雾霾盒子 TEMPER_H文件
	Email：1040459612@qq.com
	
*/
#ifndef __MYIIC_H
#define __MYIIC_H
#include "sys.h"

   	   		   
//IO方向设置
//#define SDA_IN()  {GPIOC->CRH&=0XFFFF0FFF;GPIOC->CRH|=8<<12;}
//#define SDA_OUT() {GPIOC->CRH&=0XFFFF0FFF;GPIOC->CRH|=3<<12;}

//IO操作函数	 
#define IIC_SCL    PCout(14) //SCL
#define IIC_SDA    PCout(13) //SDA	 
#define READ_SDA   PCin(13)  //输入SDA 

//IIC所有操作函数
void IIC_Init(void);               					  //初始化IIC的IO口				 
void IIC_Start(void);													//发送IIC开始信号
void IIC_Stop(void);	  											//发送IIC停止信号
void IIC_Send_Byte(u8 txd);										//IIC发送一个字节
uint8_t IIC_Read_Byte(unsigned char ack);     //IIC读取一个字节
uint8_t IIC_Wait_Ack(void); 				          //IIC等待ACK信号
void IIC_Ack(void);					                  //IIC发送ACK信号
void IIC_NAck(void);				                  //IIC不发送ACK信号

void IIC_Write_One_Byte(u8 daddr,u8 addr,u8 data);
uint8_t IIC_Read_One_Byte(u8 daddr,u8 addr);	  
#endif
















