/*
	2018-05-02 NOW
	BY 楚博豪
	IN 郑州大学物联网实验硬件组
	雾霾盒子 MYIIC_C文件
	Email：1040459612@qq.com
*/

#include "myiic.h"
#include "config.h"

/*******************************************************************************
* 函数名称  :  IIC_Init
* 函数描述  :  IIC IO初始化
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void IIC_Init(void)
{					     
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOC, ENABLE );	
	   
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
 
	IIC_SCL=1;
	IIC_SDA=1;

}

/*******************************************************************************
* 函数名称  :  SDA_OUT()
* 函数描述  :  设置PIN13为开漏输出
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void SDA_OUT()
{
    GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

/*******************************************************************************
* 函数名称  :  SDA_IN()
* 函数描述  :  设置PIN13为浮空输入
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void SDA_IN()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

/*******************************************************************************
* 函数名称  :  IIC_Start
* 函数描述  :  产生IIC起始信号
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void IIC_Start(void)
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;	  	  
	IIC_SCL=1;
	delay_us(4);
 	IIC_SDA=0;		//START:when CLK is high,DATA change form high to low 
	delay_us(4);
	IIC_SCL=0;		//钳住I2C总线，准备发送或接收数据 
	delay_us(4);
}	

/*******************************************************************************
* 函数名称  :  IIC_Stop
* 函数描述  :  产生IIC停止信号
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void IIC_Stop(void)
{
	SDA_OUT();//sda线输出
	IIC_SCL=0;
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
 	delay_us(4);
	IIC_SCL=1; 
		delay_us(1);
	IIC_SDA=1;//发送I2C总线结束信号
	delay_us(4);							   	
}


/*******************************************************************************
* 函数名称  :  IIC_Wait_Ack
* 函数描述  :  等待应答信号到来 1，接收应答失败 0，接收应答成功
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/ 
uint8_t IIC_Wait_Ack(void)
{
	uint8_t ucErrTime=0;
	SDA_IN();      //SDA设置为输入  
	IIC_SDA=1;delay_us(1);	   
	IIC_SCL=1;delay_us(1);	 
	while(READ_SDA)
	{
		ucErrTime++;
		//	delay_us(1);
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCL=0;//时钟输出0 	   
	return 0;  
} 

/*******************************************************************************
* 函数名称  :  IIC_Ack
* 函数描述  :  产生ACK应答
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void IIC_Ack(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=0;
	delay_us(20);
	IIC_SCL=1;
	delay_us(2);
	IIC_SCL=0;
}

/*******************************************************************************
* 函数名称  :  IIC_NAck
* 函数描述  :  不产生ACK应答
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
//		    
void IIC_NAck(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=1;
	delay_us(5);
	IIC_SCL=1;
	delay_us(5);
	IIC_SCL=0;
}

/*******************************************************************************
* 函数名称  :  IIC_Send_Byte
* 函数描述  :  IIC发送一个字节 1，有应答 0，无应答
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
void IIC_Send_Byte(u8 txd)
{                        
	u8 t;   
	SDA_OUT(); 	    
	IIC_SCL=0;//拉低时钟开始数据传输
	for(t=0;t<8;t++)
	{              
		IIC_SDA=(txd&0x80)>>7;
		txd<<=1; 	  
		delay_us(5);   //对TEA5767这三个延时都是必须的
		IIC_SCL=1;
		delay_us(5); 
		IIC_SCL=0;	
		delay_us(5);
	}	 
} 

/*******************************************************************************
* 函数名称  :  IIC_Read_Byte
* 函数描述  :  读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
* 输入参数  :  无
* 输出结果  :  无
* 返回的值  :  无
*******************************************************************************/
uint8_t IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0;
	SDA_IN();//SDA设置为输入
	for(i=0;i<8;i++ )
	{
		IIC_SCL=0; 
		delay_us(5);
		IIC_SCL=1;
		receive<<=1;
		if(READ_SDA)
		{
			receive++;
		}			
		delay_us(5); 
	}					 
	if (!ack)
	{
			IIC_NAck();//发送nACK
	}
	else
	{
			IIC_Ack(); //发送ACK   
	}
	return receive;
}


